import sys
import time
import os
import requests, json, urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
from datetime import date
from datetime import datetime
from dateutil import tz
from datetime import timedelta,timezone

#API

#Carbon Black Requests
envToken = '0D180AA6-18E2-4A28-907B-DCD32D375A50'


header =  {
        'X-Auth-Token' :  envToken,
        'content-type' : 'application/json'
}

b9StrongCert = False # Set to False if your Server has self-signed IIS certificate
cBbaseUrl = 'https://lvcblack01/api/bit9platform/v1/'


def cBonline():
    # Query cBlack for online assets
    header =  {
            'X-Auth-Token' :  envToken,
            'content-type' : 'application/json'
    }

    cBonline = cBbaseUrl + 'Computer?' + 'q=connected:1' + '&limit=-1'
    
    results = requests.get(cBonline, headers = header,verify=False)

    jsD = json.loads(results.content)
    jsF = json.dumps(jsD,indent=2)
    cBonlineCount = jsD["count"]

    return(cBonlineCount)

cBonlineCount = cBonline()