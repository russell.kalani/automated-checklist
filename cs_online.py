from datetime import datetime, timezone, timedelta
import requests
import sys
import csv
import time
import os
import json

#Global Variables
totalTally = []
finalCnt = 0

# Date Formats

localDt = datetime.now()
utc = datetime.now(timezone.utc)
dispDate = utc.strftime("%Y-%m-%dT%H:%M:%S.%SZ")
td = localDt + timedelta(hours=-1)

#API
cs_access_key = os.environ.get('cs_access_key')
cs_secret_key = os.environ.get('cs_secret_key')

base_url = "https://api.us-2.crowdstrike.com" 

#cs_url

# Get OAuth2 token

def get_oauth2_token():
    data_info = {
        'client_id': cs_access_key,
        'client_secret': cs_secret_key
    }
    header_info = { 
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
    }
    oauth2_token_url = base_url + '/oauth2/token'
    oauth2_token = (requests.post(oauth2_token_url, headers = header_info, data=data_info).json())['access_token']
    active_oauth2_token = str(oauth2_token)
    
    return active_oauth2_token

def get_ids():

    #Parameters for reques
    limit = 100
    limitStr = str(limit)
    offset = 1    
    offsetStr = str(offset)
    last_seen = "now-1h"

    device_url = base_url + '/devices/queries/devices/v1' + "?" +  "limit=" + limitStr + "&offset=" + offsetStr + "&filter=last_seen:>" + '"' + last_seen + '"'
    
    header_info = {

    'Authorization': 'Bearer ' + active_oauth2_token,
    'Accept': 'application/json'

    }
    
    results = requests.get(device_url, headers = header_info).json()
    total_amount = results['meta']['pagination']['total']
    offset = results['meta']['pagination']['offset']
    offsetStr = str(offset)
    limit_amount = results['meta']['pagination']['limit']
    aids = results["resources"]
    met = results["meta"]
    
    return total_amount

active_oauth2_token = get_oauth2_token()
total_amount = get_ids()
