##################################################################
#
# CrowdStrike open Incident count
# 5.12.2023
# RussK
#
##################################################################

import requests, urllib3
from datetime import datetime, timedelta, timezone
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
import sys
import time
import os
import json
from datetime import date

today = datetime.now()
utcToday = datetime.utcnow()
utcFormat = utcToday.strftime("%Y-%m-%dT%H:%M:%S.%f%Z")
dispDate = today.strftime("%d/%m/%Y %H:%M:%S")

#API
cs_access_key = '87ee70d517474e1583b0abb5e082d5d4'
cs_secret_key = 'dLhZegKNj768PfkvUmQ590BzHxE1r2tlIC3opAy4'

#cs_url

base_url = "https://api.us-2.crowdstrike.com" 

# Each endpoint requires authorization via an OAuth2 token
def get_oauth2_token():
    data_info = {
        'client_id': cs_access_key,
        'client_secret': cs_secret_key
    }
    header_info = { 
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
    }
    oauth2_token_url = base_url + '/oauth2/token'
    oauth2_token = (requests.post(oauth2_token_url, headers = header_info, data=data_info).json())['access_token']
    active_oauth2_token = str(oauth2_token)
    
    return active_oauth2_token

def get_detections():

    header_info = {
    
    'Authorization': 'Bearer ' + active_oauth2_token,
    'Accept': 'application/json'
    }
    
    device_url = base_url + "/detects/queries/detects/v1?filter=status:'new'"
    results = requests.get(device_url, headers = header_info).json()
    newDetections = results['meta']['pagination']['total']
    
    return(newDetections)

active_oauth2_token = get_oauth2_token()
newDetections = get_detections()

