import sys
import time
import os
import requests, json, urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
from datetime import date
from datetime import datetime
from dateutil import tz
from datetime import timedelta,timezone

#Other variables
today = datetime.now()
rptTime = 24
utcoffset = 7
utc_zone = tz.gettz('UTC')
local_zone = tz.tzlocal()
utcToday = datetime.utcnow()
offset = utcToday - today
utcFormat = utcToday.strftime("%Y-%m-%dT%H:%M:%SZ")
UFmt = "%Y-%m-%dT%H:%M:%SZ"
currDate = today.strftime("%m/%d/%Y %H:%M:%S")
currDay = utcToday.day
currYear = utcToday.year
currMonth = utcToday.month
beginDay = "00:00:01"
endDay = "23:59:59"

#API
#Carbon Black Requests
envToken = '0D180AA6-18E2-4A28-907B-DCD32D375A50'

header =  {
        'X-Auth-Token' :  envToken,
        'content-type' : 'application/json'
}

b9StrongCert = False # Set to False if your Server has self-signed IIS certificate
cBbaseUrl = 'https://lvcblack01/api/bit9platform/v1/'

def cBevents():

    evtList = []
    currDay = utcToday.day
    currYear = utcToday.year
    currMonth = utcToday.month
    currDaym1 = currDay - 1
    beginDay = "00:00:01Z"
    endDay = "23:59:59Z"
    recTSbegin = (f'{currYear}-{currMonth}-{currDaym1}T{beginDay}')
    recTSend = (f'{currYear}-{currMonth}-{currDaym1}T{endDay}')
    cBevent = cBbaseUrl + 'event?' + 'q=receivedTimestamp>-5m' + '&q=subtype:801' + '&sort=timestamp' + '&limit=1'

    results = requests.get(cBevent, headers = header,verify=False)
    
    jsD = json.loads(results.content)
    jsF = json.dumps(jsD,indent=2)
    #print(jsF)

    for i in jsD:
    
        evtName = i["computerName"]  
        evtId = i["id"]
        evtSubtype = i["subtype"]
        evtSubtypeName = i["subtypeName"]
        evtTimestamp = i["timestamp"]
        #Convert Z to Local
        evtUTCToLocal = date_conv(evtTimestamp)  
        

        evtList = [evtId,evtName,evtSubtypeName,evtTimestamp,evtTimestamp,evtUTCToLocal]
         
        return(evtList) 

def date_conv(timeIn):

    #print(f'In {timeIn}')
    #UFmt = "%Y-%m-%dT%H:%M:%SZ"
    lFmt = "%m/%d/%Y %H:%M:%S"
    dObj = datetime.strptime(timeIn,UFmt)
    timeIn = (dObj - offset).replace(microsecond=0)
    timeOut = datetime.strftime(timeIn,lFmt)
                                
    #print(f'Out {timeOut}')
    return timeOut

#evtUTCToLocal = date_conv(timeIn)
evtList = cBevents()
print(evtList)