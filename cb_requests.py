import sys
import time
import os
import requests, json, urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
from datetime import date
from datetime import datetime
from dateutil import tz
from datetime import timedelta,timezone

#Other variables
today = datetime.now()

rptTime = 24
#utcoffset = 7
#utc_zone = tz.gettz('UTC')
#local_zone = tz.tzlocal()
utcToday = datetime.utcnow()

utcFormat = utcToday.strftime("%Y-%m-%dT%H:%M:%SZ")
currDate = today.strftime("%m/%d/%Y %H:%M:%S")
currDay = utcToday.day
currYear = utcToday.year
currMonth = utcToday.month
beginDay = "00:00:01Z"
endDay = "23:59:59Z"

offset = utcToday - today
#print(f'{currDay}, {currMonth}, {currYear}')
utcMinusDay = (utcToday - timedelta(hours=rptTime)).strftime("%Y-%m-%dT%H:%M:%SZ")

#API
#Carbon Black Requests
envToken = '0D180AA6-18E2-4A28-907B-DCD32D375A50'
fileName = "cBlackRequest"

header =  {
        'X-Auth-Token' :  envToken,
        'content-type' : 'application/json'
}

b9StrongCert = False # Set to False if your Server has self-signed IIS certificate
cBbaseUrl = 'https://lvcblack01/api/bit9platform/v1/'


def cBcomprequests():

    currDay = utcToday.day
    currYear = utcToday.year
    currMonth = utcToday.month
    currDaym1 = currDay - 1
    recTSbegin = (f'{currYear}-{currMonth}-{currDaym1}T{beginDay}')
    recTSend = (f'{currYear}-{currMonth}-{currDaym1}T{endDay}')

    cBcomprequest = cBbaseUrl + 'approvalRequest?' + 'q=dateCreated>' + recTSbegin + '&q=dateCreated<' + recTSend  +'&q=status:3' + '&limit=-1'

    results = requests.get(cBcomprequest, headers = header,verify=False)
    
    jsD = json.loads(results.content)
    jsF = json.dumps(jsD,indent=2)
    cBcompreqCount = jsD["count"]
    

    return(cBcompreqCount) 

cBcompreqCount = cBcomprequests()