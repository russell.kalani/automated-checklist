import requests, urllib3
from datetime import datetime, timedelta, timezone
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
import sys
import time
import os
import json
from datetime import date

# Clear Screen
#os.system('cls')

today = datetime.now()
utcToday = datetime.utcnow()
utcFormated = utcToday.strftime("%Y-%m-%dT%H:%M:%S.%f%Z")
todayFormatted = today.strftime("%d/%m/%Y %H:%M:%S")

#API
fp_user = 'fp_api'
fp_key = '%cfKwh9mMX@fsN5)lQ7Wy##)#x1i5'


base_url = "https://lvfpapweb01:9443/dlp/rest/v1" 

# Each API call requires authorization via the obtained JWT token

def get_jwt_token():

    data_info = {

        'client_id': fp_user,
        'client_secret': fp_key
    }
    header_info = { 

        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json',
        'username' : fp_user,
        'password' : fp_key

    }
    jwt_token_url = base_url + '/auth/refresh-token'
    jwt_token = (requests.post(jwt_token_url, headers = header_info, data=data_info,verify=False).json())['access_token']
    active_jwt_token = str(jwt_token)
    
    

    return active_jwt_token

def get_incidents():

    # Query the FP API and gather a list of Incidents of the previous 5 minutes when the script was ran
    # Capture the most recent item for the Checklist

    incidentList = ()
    header_info = {
    
    'Authorization': 'Bearer ' + active_jwt_token,
    'Content-Type': 'application/json'

    }
    
    data_info = "{\r\n\r\n \"type\" : \"INCIDENTS\"\r\n,\r\n \"from_date\" : \""+fromDate+"\"\r\n,\r\n \"to_date\" : \""+toDate+"\"\r\n,\r\n \"sort_by\" : \"INSERT_DATE\"\r\n      \r\n}"
    
    incident_url = base_url + "/incidents"

    results = requests.post(incident_url, headers = header_info,data=data_info,verify=False)  

    jsD = json.loads(results.content)
    jsF = json.dumps(jsD,indent=2)
    fpincidentList = jsD["incidents"]
    fpMessage = "FP No ForcePoint activity in the past 60 minutes, please check the Server"
    print()

    if fpincidentList != []:
        
        retData = fpincidentList[0]        
        eventID =  retData["id"]
        incDate =  retData["incident_time"]
        eventTime = retData["event_time"]
        logName = retData["source"]["login_name"]
        hostName = retData["source"]["host_name"]
        hostName = "None"  
        fpincidentList = (eventID,incDate,eventTime,logName,hostName)
        print(f'Event ID: {eventID} Date: {incDate} Event Time: {eventTime} Login Name: {logName} HostName: {hostName}')
        fpMessage = 'Events Present'
      

    return(fpincidentList,fpMessage)

def get_dateforQuery():
 
    # Spits out the to and from dates for the query   
    today = datetime.now()
    format = "%d/%m/%Y %H:%M:%S"
    dayofWeek = today.weekday()
    weekEnd = (5,6)
    weekDay = (0,1,2,3)

    if dayofWeek == 4:

        adjDate = (today - timedelta(minutes=60)).replace(second=0)

    elif dayofWeek in weekDay:

        adjDate = (today - timedelta(minutes=60)).replace(second=0)
            
    elif dayofWeek in weekEnd:

        exit(0)

    toDate = datetime.strftime(today ,format)
    fromDate = datetime.strftime(adjDate ,format)

    return(fromDate,toDate)

active_jwt_token =  get_jwt_token()
fromDate,toDate = get_dateforQuery()
incidentList,fpMessage = get_incidents()

