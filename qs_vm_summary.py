import requests
import json
from datetime import date, datetime, timedelta
import time
from io import StringIO, BytesIO
from lxml import objectify
from lxml import etree as ET

dispDate = datetime.now().strftime("%m/%d/%Y %H:%M:%S")
utcToday = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")
utcfTobject = datetime.strptime(utcToday,"%Y-%m-%dT%H:%M:%SZ")

rptTime = 24
utcMinusDay = (utcfTobject - timedelta(hours=rptTime)).strftime("%Y-%m-%dT%H:%M:%SZ")
filePath = "/etc/grafana/grafana-metrics/SecOps/"
fileName = "qvmScanStats"

#API
qs_username = 'credt6aa4'
qs_pwd = 'DN1866gW93q#zJB!1AO(qW$4K'
api_user = 'credt6aa4 SecOps'

#cs_url
base_url = 'https://qualysapi.qg4.apps.qualys.com'

def get_dateforQuery():

    # Gather dates/times for the last five minutes.  Used to shrink the amount of data from the API
    
    convDate = ''
    utcToday = datetime.utcnow()
    format = "%Y-%m-%dT%H:%M:%SZ"
    dayofWeek = utcToday.weekday()
    weekEnd = (5,6)
    weekDay = (0,1,2,3)

    if dayofWeek == 4:

        adjDate = (utcToday - timedelta(days=3)).replace(hour=0,minute=0,second=1)

    elif dayofWeek in weekDay:

        adjDate = (utcToday - timedelta(days=1)).replace(hour=0,minute=0,second=1)
            
    elif dayofWeek in weekEnd:

        exit(0)

    convDate = datetime.strftime(adjDate ,format)

    return(convDate)

def get_scanStats():

    qsScanList = []
    tempList = ()
    
    parameters = 'action=list&scan_datetime_since=' + convDate +  '&include_hosts_summary=0' 
      
    appUrl = base_url + '/api/2.0/fo/scan/vm/summary/?' + parameters
    header_info = { 

            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json',
            'X-Requested-With': api_user
            
        }

    request = requests.get(appUrl, headers = header_info, auth=(qs_username,qs_pwd))
    
    xmlroot = ET.fromstring(request.content)
    
    #Uncomment if you want to view the xml
    #indent(xmlroot)
    #pretty_xml = ET.tostring(xmlroot, encoding="unicode")
    #print(pretty_xml)
   
    scanCnt = 0
    
    for item in xmlroot.iter('SCAN_SUMMARY'):
        #Used to count # of scans only
        item.find('.//SCAN_REFERENCE')
        #print(item.find('.//SCAN_REFERENCE').text)
        scanCnt += 1
     
    for item in xmlroot.iter('SCAN_SUMMARY'):
     
        sRef = item.find('.//SCAN_REFERENCE').text
        scTitle = item.find('.//TITLE').text
        scDate = item.find('.//SCAN_DATETIME').text
        scTarget = item.find('.//TARGETS/IP_LIST/COUNT').text
        scStatus = item.find('.//SCAN_DETAILS/STATUS').text
        scCount = item.find('.//SCAN_RESULTS/DETECTIONS/VULN/CONFIRMED/TOTAL_COUNT').text
        
        tempList = [sRef,scTitle,scDate,scTarget,scStatus]
        qsScanList.append(tempList)

    return(qsScanList,scanCnt) 

def indent(elem, level=0):
   # Add indentation
   indent_size = "  "
   i = "\n" + level * indent_size
   if len(elem):
      if not elem.text or not elem.text.strip():
         elem.text = i + indent_size
      if not elem.tail or not elem.tail.strip():
         elem.tail = i
      for elem in elem:
         indent(elem, level + 1)
      if not elem.tail or not elem.tail.strip():
         elem.tail = i
   else:
      if level and (not elem.tail or not elem.tail.strip()):
         elem.tail = i
        
convDate = get_dateforQuery()
qsScanList,scanCnt = get_scanStats()
