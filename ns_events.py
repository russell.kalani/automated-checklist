import requests, urllib3
from datetime import datetime, timedelta, timezone, date
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
import sys
import time
import os
import json
import time

#API
ns_api_key = os.environ.get('3c5e6097fa8ad6b6d25e17283781b6be')
ns_api_key_v2  = '99de02eb690f9a94d021d26754b0c087'
cs_secret_key = os.environ.get('netskope_api')

base_url = "https://creditonebank.goskope.com/" 

def get_publisher_status():
# Get publisher list and remove non-registered ones
    innerList = []
    nsEventsList = []

    nsEventsUrl = base_url + "api/v2/events/data/alert" 
    
    header_info = {
        
        'accept': 'application/json',
        'Netskope-Api-Token': ns_api_key_v2 
        
        }

    results = requests.get(nsEventsUrl,headers=header_info,verify=False)
    
    sc = results.status_code
    resultsContent = results.content
    jsD = json.loads(resultsContent)
    jsF = json.dumps(jsD,indent=2)
    pubCount = (jsD["data"]["events"])
    pubCnt  = (len(pubCount))
   
    #for x in range(pubCnt):
    #    pubName = jsD["data"]["publishers"][x]["publisher_name"]
    #    pubId = jsD["data"]["publishers"][x]["stitcher_id"]
    #    pubStat = jsD["data"]["publishers"][x]["status"]
    #    pubIP = jsD["data"]["publishers"][0]["assessment"]["ip_address"]
    #    pubVer = jsD["data"]["publishers"][0]["assessment"]["version"]
        
    #    if pubStat == 'connected':
    #        innList = [pubName,pubStat,pubIP,pubVer]
    #        nsPubList.append(innList) 
    #        x += 1
    #    else:
    #        #Skip Record
    #        continue      

    #return(nsPubList)

def date_conv(timeS):

    #print(timeS)
    UFmt = "%Y-%m-%dT%H:%M:%SZ"
    lFmt = "%m/%d/%Y %H:%M:%S"
    dObj = datetime.strptime(timeS,UFmt)
    timeS = (dObj - offset).replace(microsecond=0)
    
    return timeS

def get_dateforQuery():

   # Gather dates/times for the last five minutes.  Used to shrink the amount of data from the API
 
   utcToday = datetime.utcnow()
   format = "%Y-%m-%dT%H:%M:%SZ"
   dayofWeek = utcToday.weekday()
   weekEnd = (5,6)
   weekDay = (0,2,3,4)
   monday = (1)

   if dayofWeek == 1:

      toDate = (utcToday - timedelta(days=3)).replace(hour=0,minute=0,second=1,microsecond=0)
      fromDate = (utcToday - timedelta(days=3)).replace(hour=23,minute=59,second=59)

   elif dayofWeek in weekDay:

      toDate = (utcToday - timedelta(days=1)).replace(hour=0,minute=0,second=1,microsecond=0)
      fromDate = (utcToday - timedelta(days=1)).replace(hour=23,minute=59,second=59)
         
   elif dayofWeek in weekEnd:

      exit(0)

   #convToDate = datetime.strftime(toDate ,format)
   
   
   convToDate = datetime.strftime(toDate,format)
   
   convFromDate = datetime.strftime(fromDate ,format)
   ep = str(toDate.timestamp()).split('.',[0])

   
   print(ep)
   print(f' To: {convToDate} From: {fromDate}')
   
   return(convToDate,fromDate)
        
#nsPubList = get_publisher_status()
get_dateforQuery()