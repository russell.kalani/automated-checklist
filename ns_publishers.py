import requests, urllib3
from datetime import datetime, timedelta, timezone, date
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
import sys
import time
import os
import json
import time

#API
ns_api_key = os.environ.get('3c5e6097fa8ad6b6d25e17283781b6be')
ns_api_key_v2  = '99de02eb690f9a94d021d26754b0c087'
cs_secret_key = os.environ.get('netskope_api')

base_url = "https://creditonebank.goskope.com/" 

def get_publisher_status():
# Get publisher list and remove non-registered ones
    innList = []
    nsPubList = []
    pubStatusUrl = base_url + "api/v2/infrastructure/publishers" 
    
    header_info = {
        
        'accept': 'application/json',
        'Netskope-Api-Token': ns_api_key_v2 
        
        }

    authResp = requests.get(pubStatusUrl,headers=header_info,verify=False)
    
    sc = authResp.status_code
    respContent = authResp.content
    jsD = json.loads(respContent)
    jsF = json.dumps(jsD,indent=2)
    pubCount = (jsD["data"]["publishers"])
    pubCnt  = (len(pubCount))
   
    for x in range(pubCnt):
        pubName = jsD["data"]["publishers"][x]["publisher_name"]
        pubId = jsD["data"]["publishers"][x]["stitcher_id"]
        pubStat = jsD["data"]["publishers"][x]["status"]
        pubIP = jsD["data"]["publishers"][0]["assessment"]["ip_address"]
        pubVer = jsD["data"]["publishers"][0]["assessment"]["version"]
        
        if pubStat == 'connected':
            innList = [pubName,pubStat,pubIP,pubVer]
            nsPubList.append(innList) 
            x += 1
        else:
            #Skip Record
            continue      

    return(nsPubList)
        
nsPubList = get_publisher_status()