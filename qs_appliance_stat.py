import requests
from datetime import date, datetime, timedelta
import time
import sys
import os
from lxml import objectify
from lxml import etree as ET

dispDate = datetime.now().strftime("%m/%d/%Y %H:%M:%S")
utcToday = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")
utcfTobject = datetime.strptime(utcToday,"%Y-%m-%dT%H:%M:%SZ")
rptTime = 24
utcMinusDay = (utcfTobject - timedelta(hours=rptTime)).strftime("%Y-%m-%dT%H:%M:%SZ")
filePath = "/etc/grafana/grafana-metrics/SecOps/"
#filePath = "C:\\temp\\"
fileName = "qDLscannerStat"

#API
qs_username = 'credt6aa4'
qs_pwd = 'DN1866gW93q#zJB!1AO(qW$4K'
api_user = 'credt6aa4 SecOps'

#cs_url
base_url = 'https://qualysapi.qg4.apps.qualys.com'
        
def get_applianceStat():

    qs_appList = []
    tempList = ()
    parameters = 'action=list&output_mode=brief&scan_detail=1&include_license_info=0'
    appUrl = base_url + '/api/2.0/fo/appliance/?' + parameters
    header_info = { 

        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json',
        'X-Requested-With': api_user
        
    }

    request = requests.get(appUrl, headers = header_info, auth=(qs_username,qs_pwd))
   
    xml = ET.fromstring(request.content)
    
    qs_applianceCount = (len(xml.findall('.//APPLIANCE')))
    
    #print(request.content)

    for item in xml.iter(tag='APPLIANCE'):
        
        name = item.find('./NAME').text
        status = item.find('./STATUS').text
        running = item.find('.//RUNNING_SCAN_COUNT').text
        version = item.find('./SOFTWARE_VERSION').text

        tempList = (name,status,running,version)
        qs_appList.append(tempList)
    

    return(qs_appList,qs_applianceCount)   

qs_appList,qs_applianceCount = get_applianceStat()