#
# Automated Checklist
# 8.14.2023
# RussK
#

import requests, json, urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
import sys
import time
import os
from datetime import date
from datetime import datetime
from dateutil import tz
from datetime import timedelta,timezone
import cs_online
import cs_detections
import cs_incidents
import qs_agent_count
import qs_appliance_stat
import qs_vm_summary
import fp_incident
import ns_publishers
import cb_online
import cb_requests
import cb_localapproval
import cb_events

# Clear Screen
os.system('cls')

# Counter
total_entries = 0

#Other variables
today = datetime.now()
rptTime = 24
utcoffset = 7
utc_zone = tz.gettz('UTC')
local_zone = tz.tzlocal()
utcToday = datetime.utcnow()

utcFormat = utcToday.strftime("%Y-%m-%dT%H:%M:%SZ")
currDate = today.strftime("%m/%d/%Y %H:%M:%S")
currDay = utcToday.day
currYear = utcToday.year
currMonth = utcToday.month
beginDay = "00:00:01"
endDay = "23:59:59"

offset = utcToday - today
utcMinusDay = (utcToday - timedelta(hours=rptTime)).strftime("%Y-%m-%dT%H:%M:%SZ")

filePath = "C:\\Temp\\"
fileName = "daily"

#API
#Crowdstrike Keys
#cs_access_key = os.environ.get('cs_access_key')
#cs_secret_key = os.environ.get('cs_secret_key')
#Carbon Black Requests

def date_conv(timeS):

    #print(timeS)
    UFmt = "%Y-%m-%dT%H:%M:%SZ"
    lFmt = "%m/%d/%Y %H:%M:%S"
    dObj = datetime.strptime(timeS,UFmt)
    timeS = (dObj - offset).replace(microsecond=0)
    
    return timeS

# Grab data with these mini scripts
cBonlineCount = cb_online.cBonline()
cBcompreqCount = cb_requests.cBcomprequests()
cBlocalappCount = cb_localapproval.cBlocalApp()
evtList = cb_events.cBevents()
total_amount = cs_online.get_ids()
newDetections = cs_detections.get_detections()
get_incident = cs_incidents.get_incident()
qsAgentCount = qs_agent_count.qs_agent()
#qsAppList,qs_applianceCount = qs_appliance_stat.get_applianceStat()
#qsScanList,qs_scanCount = qs_vm_summary.get_scanStats()
#fpIncidentList,fpMessage = fp_incident.get_incidents()
#nsPublist = ns_publishers.get_publisher_status()



print("")
print(f'------------ Carbon Black ------------------------')
print(f'cB Online Assets:  {cBonlineCount}')
print(f'cB Requests     :  {cBcompreqCount}')
print(f'cB Local Approval: {cBlocalappCount}')
print(f'cB Completed requests (Last day): {cBcompreqCount}')
#print(evtList)
print(f'cB Last Event:  HN: {evtList}  ID: {evtList[1]}  Subtype: {evtList[2]} Subtype Name: {evtList[3]}  Timestamp: {evtList[4]}  Time to Local: {evtList[5]}')
print(f'------------ CrowdStrike --------------------------')
print(f'CS Online Assets: {total_amount}')
print(f'CS Detections: {newDetections}')
print(f'CS Incidents: {get_incident}')
print(f'------------ Qualys --------------------------')
print(f'QS Agent Count: {qsAgentCount}')


#Loop through Qualys Server List
qs_servers = len(qsAppList)
qsCnt = 0
innerList = []
qsPublist = []

while qsCnt != qs_servers:
    qs_ServName = qsAppList[qsCnt][0]
    qs_ServStat = qsAppList[qsCnt][1]
    qs_ServVer = qsAppList[qsCnt][3]
    qs_ServCnt = qsAppList[qsCnt][2]
    qsCnt += 1

    innerList = [qs_ServName,qs_ServStat,qs_ServVer,qs_ServCnt]
    qsPublist.append(innerList)

print(f'QS Server Count: {qs_applianceCount}')

#------------- QS VM Scan List -------------------#

qsScanCnt = len(qsScanList)
innerList = []
qsVMlist = []
qsCnt = 0

print(f'QS VM Scan Count: {qsScanCnt}')

while qsCnt != qsScanCnt:

    print(qsCnt)
    qsScanId = qsScanList[qsCnt][0]
    qsScanTitle = qsScanList[qsCnt][1]
    qsScanStatus = qsScanList[qsCnt][2]
    qsScanDate = qsScanList[qsCnt][3]
    qsScanDetect = qsScanList[qsCnt][4]
    
    innerList = [qsScanId,qsScanTitle,qsScanStatus,qsScanDate,qsScanDetect]
    qsVMlist.append(innerList)

#print(f'QS Total Scans {scanCnt}')
print(f'QS Scan Name: {qsVMlist[0]}, ID: {qsScanTitle} Status: {qsScanStatus} Scan Date: {qsScanDate} Detections: {qsScanDetect}')
    

    #ForcePoint
fpMessage = "No ForcePoint activity in the past 60 minutes, please check the Server"

if fpIncidentList != []:

    print(f'------------ ForcePoint --------------------------')
    #print(f'FP Last Incident: Event ID:{fpIncidentList[0]}  Date:{fpIncidentList[1]}  Event Date: {fpIncidentList[2]}  User: {fpIncidentList[3]} Host: {fpIncidentList[2]}')

else:
    print(fpMessage)

#NS Publisher Status
print(f'------------ Netskope ----------------------------')
innerList = []
pubFinal = []

pubListcnt = len(nsPublist)
pubCnt = 0

while pubCnt != pubListcnt:

    pubName = nsPublist[pubCnt][0]
    pubStat = nsPublist[pubCnt][1]
    pubIP =  nsPublist[pubCnt][2]
    pubVer = nsPublist[pubCnt][3]
    print(f'NS Publisher Name: {pubName} Status: {pubStat} IP: {pubIP} Version: {pubVer}')

    #pubCnt += 1
    innerList = [pubName,pubStat,pubIP,pubVer]
    pubFinal.append(innerList)

print()
print("--------Script Finished -----------")
sys.exit(0)